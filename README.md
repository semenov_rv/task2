# README #
This program reads a file, sorts the words of your choice, and writes the result to a new file.

On the command line you need to type 6 arguments:
RequiredOption :
1. -i The name of the file from which the program will take data.
2. -o The name of the file where the program will write the result.
3. -s The name of the class, which implements the required sort type.
Not Required:
4. -r Call reversed type sorted (Optional argument)
5. -a Call ALL available sorters at once.
6. -t Set the number of threads

Available sorters:
1. AlphabetSorted - sorts words alphabetically.
2. LengthSorted - sorts words by a length.
3. FlipWordsAlphabetSorted - sorts flip words (astra -> artsa).
4. HashCodeSorted - sorts words by a hashcode.
5. LastCharSorted - sorts words by a last char.
6. HalfWordSorted - sorts by the second half of the word.

Example use case:

java -jar Task2.jar -i "Poem" -o "Result" -s LengthSorted  

if you want call reverse type sort:

java -jar Task2.jar -i "Poem" -o "Result" -s LengthSorted  -r 

Call all sorted and reverse type, set  thread  count 4

java -jar Task2.jar -i "Poem" -o "Result" -s ""  -r -a  -t 4.






