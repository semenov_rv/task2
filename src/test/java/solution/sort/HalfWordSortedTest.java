package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HalfWordSortedTest {

    @Test
    void sorted() {
        Stream<String> testStream = Stream.of("Down", "Sun", "Astra");
        List<String> expected = Arrays.asList("astra                    | : tra", "sun                      | : un", "down                     | : wn");

       HalfWordSorted halfWordSorted = new HalfWordSorted();
        List<String> actual = halfWordSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}