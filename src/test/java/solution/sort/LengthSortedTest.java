package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class LengthSortedTest {
    @Test
    void sorted() {
        Stream<String> testStream = Stream.of("Down", "Sun", "Astra");
        List<String> expected = Arrays.asList("sun                      | : 3", "down                     | : 4", "astra                    | : 5");

        LengthSorted lengthSorted = new LengthSorted();
        List<String> actual = lengthSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}