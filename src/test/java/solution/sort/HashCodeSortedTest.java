package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HashCodeSortedTest {

    @Test
    void sorted() {
        Stream<String> testStream = Stream.of("Down", "Sun", "Astra");
        List<String> expected = Arrays.asList("sun                      | : 114252", "down                     | : 3089570", "astra                    | : 93122609");

        HashCodeSorted hashCodeSorted = new HashCodeSorted();
        List<String> actual = hashCodeSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}