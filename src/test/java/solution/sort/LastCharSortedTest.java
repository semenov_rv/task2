package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LastCharSortedTest {

    @Test
    void sorted() {
        String str1 = "Sun";
        String str2 = "Down";
        String str3 = "Astra";
        Stream<String> testStream = Stream.of(str1, str2, str3);
        List<String> expected = Arrays.asList("astra                    | : " + str3.charAt(str3.length() - 1),
                "sun                      | : " + str1.charAt(str1.length() - 1),
                "down                     | : " + str2.charAt(str2.length() - 1));

        LastCharSorted lastCharSorted = new LastCharSorted();
        List<String> actual = lastCharSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}
