package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FlipWordAlphabetSortedTest {

    @Test
    void sorted() {
        Stream<String> testStream = Stream.of("Down", "Sun", "Astra");
        List<String> expected = Arrays.asList("astra                    | : artsa", "sun                      | : nus", "down                     | : nwod");

        FlipWordAlphabetSorted flipWordAlphabetSorted = new FlipWordAlphabetSorted();
        List<String> actual = flipWordAlphabetSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}