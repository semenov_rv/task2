package solution.sort;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class AlphabetSortedTest {

    @Test
    void sorted() {
        Stream<String> testStream = Stream.of("Down", "Braun", "City", "Astra");
        List<String> expected = Arrays.asList("astra                    | : astra", "braun                    | : braun", "city                     | : city",
                "down                     | : down");

        AlphabetSorted alphabetSorted = new AlphabetSorted();
        List<String> actual = alphabetSorted.sorted(testStream);

        assertLinesMatch(expected, actual);
    }
}