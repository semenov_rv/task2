package solution;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import solution.sort.AbstractSortedClass;
import solution.sort.SortExecutor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertLinesMatch;

class MainTest {

    @Test
    void testMain()  {
        OptionsHandler optionsHandler = Mockito.mock(OptionsHandler.class);
        SortersFactory sortersFactory = new SortersFactory(optionsHandler.getReverseType());
        SortExecutor sortExecutor = new SortExecutor(sortersFactory, optionsHandler.getAvailableClasses(), optionsHandler.getAllSorted(),
                optionsHandler.getSortedClassName(), optionsHandler.getThreadPoolSize());

        List<String> actual = null;
        List<String> expected = Arrays.asList("sun                      | : 3", "down                     | : 4", "astra                    | : 5");

        try (Stream<String> lines = Files.lines(Paths.get("test.txt"))) {
            if (true) {
                sortExecutor.startAllAvailableSorted(Paths.get("test.txt"), Paths.get("out.txt"));
            } else {
                AbstractSortedClass abstractSortedClass = (AbstractSortedClass) sortersFactory.newInstance(sortExecutor.getListOfSortersName().get(0));
                abstractSortedClass.setFlag(true);
                actual = abstractSortedClass.sorted(lines);
                sortExecutor.writeToFile(Paths.get("out.txt"), actual, abstractSortedClass);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertLinesMatch(expected, actual);
    }
}
