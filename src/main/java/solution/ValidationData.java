package solution;

import solution.sort.Sortable;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ValidationData {

    public void validateOutputFile(String output) throws ValidateException {
        try {
            Files.deleteIfExists(Paths.get(output));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        if (output.isEmpty()) {
            throw new ValidateException("Вы ввели пустое имя конечного файла. Введите корректное имя.");
        }
        System.out.println("Данные будут записаны в " + output);
    }

    public void validateInputFile(String input) throws ValidateException {
        if (Files.isDirectory(Paths.get(input))) {
            throw new ValidateException("Указанный файл - является директорией!");
        }
        if (input.isEmpty()) {
            throw new ValidateException("Вы ввели пустое имя файла. Введите верное имя файла.");
        }
        if (Files.notExists(Paths.get(input))) {
            throw new ValidateException("Входной файл не найден. Попробуйте еще раз!");
        }
        if (!Files.isReadable(Paths.get(input))) {
            throw new ValidateException("Ошибка! Файл не читается!");
        }
    }

    public void validateCallClass(String callClass) throws ValidateException {
        try {
            Class<?> myClass = Class.forName(callClass);
            Class<Sortable> sortable = Sortable.class;
            if (myClass.isInterface()) {
                throw new ValidateException("Данный класс является интерфейсом.");
            }
            if (Modifier.isAbstract(myClass.getModifiers())) {
                throw new ValidateException("Данный класс является абстрактным.");
            }
            if (!sortable.isAssignableFrom(myClass)) {
                throw new ValidateException(callClass + " не имплемнтирует интерфейс Sortable.");
            }
            if (Stream.of(myClass.getConstructors())
                    .noneMatch(constructor -> constructor.getParameterCount() == 0)) {
                throw new ValidateException("Данный класс не может быть инстанцирован, т.к отсутствует конструктор без параметров.");
            }
        } catch (ClassNotFoundException e) {
            throw new ValidateException("Данного класса не существует.");
        }
    }

    public void validateThreadPoolSize(int value) throws ValidateException {
        if (value < 1) {
            throw new ValidateException("Количество потоков не может быть меньше или равным нулю !");
        }
    }
}
