package solution.sort;

public class HalfWordSorted extends AbstractSortedClass<String> {
    @Override
    String getValueMapper(String str) {
        return str.substring(str.length() / 2);
    }
}
