package solution.sort;

public class FlipWordAlphabetSorted extends AbstractSortedClass <String>{
    @Override
    String getValueMapper(String str) {
        return new StringBuilder(str).reverse().toString();
    }
}

