package solution.sort;

public class LengthSorted extends AbstractSortedClass<Integer> {
    @Override
    Integer getValueMapper(String str) {
        return str.length();
    }
}

