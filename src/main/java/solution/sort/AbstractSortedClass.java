package solution.sort;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public abstract class AbstractSortedClass<T extends Comparable<? super T>> implements Sortable {

    private boolean flag;

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    protected AbstractSortedClass() {
    }

    @Override
    public List<String> sorted(Stream<String> stream) {
        return stream
                .map(o1 -> o1.split("[\\P{L}]+"))
                .flatMap(Arrays::stream)
                .map(String::toLowerCase)
                .distinct()
                .filter(e -> !e.isEmpty())
                .map(word -> new AbstractMap.SimpleEntry<>(word, getValueMapper(word)))
                .sorted(getEntryComparator())
                .map(e -> String.format("%-25s| : %s", e.getKey(), e.getValue()))
                .collect(Collectors.toList());
    }

    public Comparator<Map.Entry<String, T>> getEntryComparator() {
        if (flag) {
            return Map.Entry.comparingByValue(Comparator.reverseOrder());
        } else {
            return Map.Entry.comparingByValue(Comparator.naturalOrder());
        }
    }

    abstract T getValueMapper(String str);
}
