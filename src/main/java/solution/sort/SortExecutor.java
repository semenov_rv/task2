package solution.sort;

import solution.ApplicationException;
import solution.SortersFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class SortExecutor {

    private final SortersFactory sortersFactory;
    private final List<Class<? extends Sortable>> listOfAvailableSorters;
    private final boolean getAllSorted;
    private final String sortersClassName;
    private final int threadPoolSize;

    public SortExecutor(SortersFactory sortersFactory, List<Class<? extends Sortable>> listOfAvailableSorters, boolean getAllSorted, String sortersClassName, int threadPoolSize) {
        this.sortersFactory = sortersFactory;
        this.listOfAvailableSorters = listOfAvailableSorters;
        this.getAllSorted = getAllSorted;
        this.sortersClassName = sortersClassName;
        this.threadPoolSize = threadPoolSize;
    }

    public void startAllAvailableSorted(Path inputFile, Path outputFile) {
        ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
        System.out.println("Вызвано потоков - " + threadPoolSize);

        CompletableFuture<?>[] completableFutures;
        try {
            completableFutures = getListOfSortersName().stream()
                    .map(sorters -> CompletableFuture.runAsync(() -> {
                        executeSorting(inputFile, outputFile, sorters);
                    }, executorService)).toArray(CompletableFuture[]::new);
        } catch (ApplicationException e) {
            throw new ApplicationException("Ошибка при попытке вызвать параллельную обработку данных.");
        } finally {
            executorService.shutdown();
        }

        CompletableFuture.allOf(completableFutures).join();
    }


    private void executeSorting(Path inputFile, Path outputFile, Class<? extends Sortable> sorters) {
        try {
            Sortable sortable = sortersFactory.newInstance(sorters);
            try (Stream<String> lines = Files.lines(inputFile)) {
                List<String> resultList = sortable.sorted(lines);
                Files.deleteIfExists(Paths.get(outputFile + sortable.getClass().getSimpleName() + ".txt"));
                writeToFile(Paths.get(outputFile + sortable.getClass().getSimpleName() + ".txt"), resultList, sortable);
            }
        } catch (IOException e) {
            throw new ApplicationException("Ошибка в блоке инициализации класса и обработки данных.");
        }
    }

    public List<Class<? extends Sortable>> getListOfSortersName() {
        if (!getAllSorted) {
            listOfAvailableSorters.removeIf(allSorters -> !allSorters.getName().equals(sortersClassName));
        }
        return listOfAvailableSorters;
    }

    public void writeToFile(Path path, List<String> list, Sortable sortable) {
        try {
            Files.write(path, list, StandardCharsets.UTF_8);
            System.out.println(sortable.getClass().getSimpleName() + " : -  успешно записал результат в файл.");
        } catch (IOException e) {
            throw new ApplicationException("Ошибка записи в файл.");
        }
    }
}
