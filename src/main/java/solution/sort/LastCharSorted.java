package solution.sort;

public class LastCharSorted extends AbstractSortedClass<Character> {
    @Override
    Character getValueMapper(String str) {
        return str.charAt(str.length()-1);
    }
}
