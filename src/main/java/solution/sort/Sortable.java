package solution.sort;

import java.util.List;
import java.util.stream.Stream;

public interface Sortable {
    List<String> sorted(Stream<String> stream);
}
