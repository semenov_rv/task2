package solution.sort;

public class HashCodeSorted extends AbstractSortedClass<Integer> {
    @Override
    Integer getValueMapper(String str) {
        return str.hashCode();
    }
}
