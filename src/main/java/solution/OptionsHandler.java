package solution;

import org.apache.commons.cli.*;
import org.reflections.Reflections;
import solution.sort.Sortable;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.stream.Collectors;

public class OptionsHandler {
    CommandLineParser parser = new DefaultParser();
    CommandLine cmd = null;
    ValidationData validationData = new ValidationData();


    public void initOptions(String[] args) throws ValidateException {
        Options options = new Options();
        OptionsContainer[] values = OptionsContainer.values();

        for (OptionsContainer optionsContainer : values) {
            Option option = new Option(optionsContainer.getOpt(), optionsContainer.getLongOpt(), optionsContainer.hasArg(), optionsContainer.getDescription());
            option.setRequired(optionsContainer.isRequired());
            options.addOption(option);
        }

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println("\n\nОшибка. Вы ввели не корректные параметры для команд.");
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("This program reads a file, sorts the words of your choice, and writes the result to a new file.\n" +
                    "\n" +
                    "\n" +
                    "Available sorters:\n\n" +
                    getAvailableClasses() +
                    "\n" + "\n" +
                    "Example use case:\n" +
                    "\n" +
                    "java -jar Task2.jar -i \"Poem\" -o \"Result\" -s LengthSorted  \n" +
                    "\n" +
                    "if you want call reverse type sort: \n" +
                    "\n" +
                    "java -jar Task2.jar -i \"Poem\" -o \"Result\" -s LengthSorted  -r\n\n" +
                    "if you want call all available sorters : \n" +
                    "\n" +
                    "java -jar Task2.jar -i \"Poem\" -o \"Result\" -s \"\"  -r -a  -t 4.\n\n", options);
            throw new ValidateException(e.getMessage());
        }
    }

    public List<Class<? extends Sortable>> getAvailableClasses() {
        Reflections reflections = new Reflections("solution.sort");
        return reflections
                .getSubTypesOf(Sortable.class)
                .stream()
                .filter(elem -> !Modifier.isAbstract(elem.getModifiers()))
                .collect(Collectors.toList());
    }

    public String getInputFile() {
        String inputFile = null;
        if (cmd.hasOption(OptionsContainer.INPUT.getOpt())) {
            inputFile = cmd.getOptionValue(OptionsContainer.INPUT.getOpt());
        } else if (cmd.hasOption(OptionsContainer.INPUT.getLongOpt())) {
            inputFile = cmd.getOptionValue(OptionsContainer.INPUT.getLongOpt());
        }
        try {
            validationData.validateInputFile(inputFile);
        } catch (ValidateException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        return inputFile;
    }

    public String getOutputFile() {
        String outputFile = null;
        if (cmd.hasOption(OptionsContainer.OUTPUT.getOpt())) {
            outputFile = cmd.getOptionValue(OptionsContainer.OUTPUT.getOpt());
        } else if (cmd.hasOption(OptionsContainer.OUTPUT.getLongOpt())) {
            outputFile = cmd.getOptionValue(OptionsContainer.OUTPUT.getLongOpt());
        }
        try {
            validationData.validateOutputFile(outputFile);
        } catch (ValidateException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        return outputFile;
    }

    public String getSortedClassName() {
        String myClassName = "";
        if (!getAllSorted()) {
            if (cmd.hasOption(OptionsContainer.SORTED_CLASS_NAME.getOpt())) {
                myClassName = cmd.getOptionValue(OptionsContainer.SORTED_CLASS_NAME.getOpt());
            } else if (cmd.hasOption(OptionsContainer.SORTED_CLASS_NAME.getLongOpt())) {
                myClassName = cmd.getOptionValue(OptionsContainer.SORTED_CLASS_NAME.getLongOpt());
            }
            try {
                validationData.validateCallClass(myClassName);
            } catch (ValidateException e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }
        }
        return myClassName;
    }

    public int getThreadPoolSize() {
        int value = 1;
        if (cmd.hasOption(OptionsContainer.THREAD_POOL_SIZE.getOpt())) {
            value = Integer.parseInt(cmd.getOptionValue(OptionsContainer.THREAD_POOL_SIZE.getOpt()));
        }
        try {
            validationData.validateThreadPoolSize(value);
        } catch (ValidateException e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        return value;
    }

    public boolean getReverseType() {
        return cmd.hasOption(OptionsContainer.REVERSE_TYPE.getOpt());
    }

    public boolean getAllSorted() {
        return cmd.hasOption(OptionsContainer.GET_ALL_REVERSE.getOpt());
    }
}
