package solution;

import solution.sort.AbstractSortedClass;
import solution.sort.Sortable;

public class SortersFactory {

    private final boolean reverseType;

    public SortersFactory(boolean reverseType) {
        this.reverseType = reverseType;
    }

    public <T> Sortable newInstance(Class<? extends  Sortable> sorters)  {
        AbstractSortedClass<? extends Comparable<? super T>> abstractSortedClass;
        try {
            abstractSortedClass = (AbstractSortedClass<? extends Comparable<? super T>>) sorters.newInstance();
        } catch (InstantiationException | IllegalAccessException  e) {
            throw new ApplicationException("Ошибка в создании класса.");
        }
        abstractSortedClass.setFlag(reverseType);
        return abstractSortedClass;
    }
}





