package solution;

import solution.sort.SortExecutor;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try {
            System.out.println("------------------------------------");
            System.out.println("Программа начала работу.");
            System.out.println("------------------------------------");

            OptionsHandler optionsHandler = new OptionsHandler();
            optionsHandler.initOptions(args);

            SortersFactory sortersFactory = new SortersFactory(optionsHandler.getReverseType());
            SortExecutor sortExecutor = new SortExecutor(sortersFactory,
                    optionsHandler.getAvailableClasses(),
                    optionsHandler.getAllSorted(),
                    optionsHandler.getSortedClassName(),
                    optionsHandler.getThreadPoolSize());

            Path inputFile = Paths.get(optionsHandler.getInputFile());
            Path outputFile = Paths.get(optionsHandler.getOutputFile());

            sortExecutor.startAllAvailableSorted(inputFile, outputFile);

            System.out.println("------------------------------------");
            System.out.println("Программа успешно завершила работу.");
            System.out.println("------------------------------------");
        } catch (Exception e) {
            System.out.println("Ошибка! Программа завершает свою работу некорректно!");
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.out.println("*** Время работы программы : " + (System.currentTimeMillis() - startTime) + " ms ***");
    }
}
