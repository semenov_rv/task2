package solution;

//todo use enum fields in OptionsHandler
public enum OptionsContainer {
    INPUT("i", "inputFile", true, "File to read.", true),
    OUTPUT("o", "outputFile", true, "File to write.", true),
    SORTED_CLASS_NAME("s", "class", true, "Call sorted class", true),
    REVERSE_TYPE("r", "reverse", false, "Call reverse sorter type.", false),
    GET_ALL_REVERSE("a", "allSorted", false, "Call all sorted", false),
    THREAD_POOL_SIZE("t", "thread", true, "Get Numbers of thread.", false);

    private final String opt;
    private final String longOpt;
    private final boolean hasArg;
    private final String description;
    private final boolean required;

    OptionsContainer(String opt, String longOpt, boolean hasArg, String description, boolean required) {
        this.opt = opt;
        this.longOpt = longOpt;
        this.hasArg = hasArg;
        this.description = description;
        this.required = required;
    }

    public String getOpt() {
        return opt;
    }

    public String getLongOpt() {
        return longOpt;
    }

    public boolean hasArg() {
        return hasArg;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRequired() {
        return required;
    }
}
